from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

driver = webdriver.Firefox()
driver.get("https://gitlab.com")
assert "DevOps Platform Delivered as a Single Application" in driver.title
button_install = driver.find_element_by_xpath('/html/body/header/div[2]/ul/li[7]/a')
button_install.click()

button_ubuntu = WebDriverWait(driver, 3).until(
    EC.presence_of_element_located((By.XPATH, '//*[@class="tile-title" and text()[contains(.,\'Ubuntu\')]]')))
button_ubuntu.click()

gitlab_instruction = WebDriverWait(driver, 3).until(
    EC.presence_of_element_located((By.XPATH, '/html/body/div[2]/div/div/div[1]/div/ul/li[1]/div[2]/pre[3]')))
assert gitlab_instruction.get_attribute("textContent").strip() == \
       'curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.deb.sh | sudo bash'

driver.close()
